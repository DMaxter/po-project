package sth;

import java.util.List;
import java.util.ArrayList;

import sth.exceptions.SurveyException;

class CreatedState extends SurveyState{
	private static final long serialVersionUID = 201812040124L;

	public CreatedState(Survey survey){
		super(survey);
	}

	public void cancel(){
		this.survey.getProject().removeSurvey();
	}

	public void open(){
		this.survey.setState(new OpenedState(this.survey));
	}

	public void close() throws SurveyException{
		throw new SurveyException();
	}

	public void finish() throws SurveyException{
		throw new SurveyException();	
	}

	public String getStatus(){
		return "(por abrir)";
	}
}
