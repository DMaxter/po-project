package sth;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.text.Collator;

import sth.exceptions.TooManyDisciplinesException;
import sth.exceptions.InvalidDisciplineException;
import sth.exceptions.InvalidProjectException;
import sth.exceptions.SurveyException;

/*
 * Student implementation
 */

class Student extends Person{
	private static final long serialVersionUID = 201811142040L;
	private static final int MAX_DISCIPLINES = 6;

	private Course course;
	private HashMap<String, Discipline> disciplines = new HashMap<String, Discipline>();

	public Student(String name, int id, String phone){
		super(name, id, phone);
	}

	public void setCourse(Course course){
		this.course = course;
	}

	public boolean isRepresentative(){
		return this.course.isRepresentative(this);
	}

	public void addDiscipline(Discipline discipline) throws TooManyDisciplinesException{
		if(this.disciplines.size() == MAX_DISCIPLINES){
			throw new TooManyDisciplinesException();
		}
		
		this.disciplines.put(discipline.getName(), discipline);
	}

	private Discipline getDiscipline(String discipline){
		return this.disciplines.get(discipline);
	}

	public void deliverProject(String disciplineName, String project, String message) throws InvalidDisciplineException, InvalidProjectException{
		Discipline discipline = this.disciplines.get(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		discipline.deliverProject(this.getId(), project, message);
	}

	public List<String> showSurveyResults(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		return discipline.showSurveyResults(this, project);
	}

	public void createSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.course.createSurvey(discipline, project);
	}

	public void cancelSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.course.cancelSurvey(discipline, project);
	}

	public void closeSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.course.closeSurvey(discipline, project);
	}

	public void finishSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.course.finishSurvey(discipline, project);
	}

	public void openSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.course.openSurvey(discipline, project);
	}

	public void answerSurvey(int id, String disciplineName, String project, int hours, String comment) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		discipline.answerSurvey(id, project, hours, comment);
	}

	public List<String> showDisciplineSurveys(String discipline) throws InvalidDisciplineException{
		return this.course.showDisciplineSurveys(discipline);
	}

	public String printDisciplines(Printer printer){
		String message = "";

		List<String> disciplines = new ArrayList<String>();

		for(Discipline discipline: this.disciplines.values()){
			 disciplines.add(discipline.print(printer));
		}

		disciplines.sort(Collator.getInstance(Locale.getDefault()));
		
		for(String s: disciplines){
			message += s + "\n";
		}

		return message.trim();
	}

	public boolean isStudent(){
		return true;
	}
}
