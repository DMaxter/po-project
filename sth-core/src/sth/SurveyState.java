package sth;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

import sth.exceptions.SurveyException;

abstract class SurveyState implements Serializable{
	private static final long serialVersionUID = 201812040124L;

	Survey survey;

	public SurveyState(Survey s){
		this.survey = s;
	}

	public abstract void cancel() throws SurveyException;
	public abstract void open() throws SurveyException;
	public abstract void close() throws SurveyException;
	public abstract void finish() throws SurveyException;
	public abstract String getStatus();

	public List<Integer> gatherResults(){
		// Empty list
		return new ArrayList<Integer>();
	}

	public void answer(int id, int hours, String comments) throws SurveyException{
		throw new SurveyException();
	}
}
