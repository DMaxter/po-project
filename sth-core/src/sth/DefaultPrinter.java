package sth;

class DefaultPrinter implements Printer{
	private char SEPARATOR = '|';
	public static final DefaultPrinter INSTANCE = new DefaultPrinter();

	private DefaultPrinter(){}

	public String print(Person person){
		String role;

		if(person.isStudent()){
			if(person.isRepresentative()){
				role = "DELEGADO";
			}else{
				role = "ALUNO";
			}

		}else if(person.isProfessor()){
			role = "DOCENTE";
		}else{
			role = "FUNCIONÁRIO";
		}

		String message = role + this.SEPARATOR + person.getId() + this.SEPARATOR + person.getPhone() + this.SEPARATOR + person.getName();

		message += "\n" + person.printDisciplines(DefaultPrinter.INSTANCE);

		return message.trim();
	}

	public String print(Discipline discipline){
		String message = "* " + discipline.getCourseName() + " - " + discipline.getName();

		return message; 
	}
}
