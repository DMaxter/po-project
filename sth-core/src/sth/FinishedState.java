package sth;

import java.util.List;
import java.util.ArrayList;

import sth.exceptions.SurveyException;
import sth.exceptions.FinishedSurveyException;

class FinishedState extends SurveyState{
	private static final long serialVersionUID = 201812040124L;

	public FinishedState(Survey survey){
		super(survey);
		String message = "Resultados do inquérito do projecto " + this.survey.getProjectName() + " da disciplina " + this.survey.getDisciplineName();
		this.survey.notifyObservers(message);
	}

	public void cancel() throws SurveyException{
		throw new FinishedSurveyException();
	}

	public void open() throws SurveyException{
		throw new SurveyException();
	}

	public void close() throws SurveyException{
		throw new SurveyException();
	}

	public void finish(){
		// left blank on purpose
	}

	public String getStatus(){
		return "";
	}

	public List<Integer> gatherResults(){
		List<Integer> results = new ArrayList<Integer>();

		results.add(this.survey.getMin());
		results.add(this.survey.getMax());
		results.add(this.survey.getAvg());
		results.add(this.survey.getSubmissions());

		return results;
	}
}
