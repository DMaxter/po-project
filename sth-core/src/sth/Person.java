package sth;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

/*
 * Person implementation
 */

abstract class Person implements Serializable, Observer, Printable{
	private static final long serialVersionUID = 201811142040L;

	private String name;
	private String phone;
	private int id;
	private List<String> inbox = new ArrayList<String>();

	public Person(String name, int id, String phone){
		this.name = name;
		this.id = id;
		this.phone = phone;
	}

	public String getName(){
		return this.name;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return this.phone;
	}

	public int getId(){
		return this.id;
	}

	public boolean isAdministrative(){
		return false;
	}

	public boolean isStudent(){
		return false;
	}

	public boolean isProfessor(){
		return false;
	}

	public boolean isRepresentative(){
		return false;
	}

	public String printDisciplines(Printer printer){
		return ""; 
	}

	public void addNotification(String notification){
		this.inbox.add(notification);
	}

	public List<String> getNotifications(){
		List<String> notifications = this.inbox;
		this.inbox = new ArrayList<String>();

		return notifications;
	}

	public String print(Printer printer){
		return printer.print(this);
	}

	public boolean equals(Object object){
		if(object instanceof Person){
			Person person = (Person) object;
			return (this.name.equals(person.getName()) &&
					this.phone.equals(person.getPhone()) &&
					this.id == person.getId());
		}

		return false;
	}
}
