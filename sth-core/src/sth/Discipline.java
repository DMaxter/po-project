package sth;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.HashMap;
import java.util.Locale;
import java.text.Collator;
import java.io.Serializable;

import sth.exceptions.TooManyStudentsException;
import sth.exceptions.InvalidProjectException;
import sth.exceptions.SurveyException;

/*
 * Discipline implementation
 */

class Discipline implements Serializable, Observable, Printable{
	private static final long serialVersionUID = 201811142040L;

	private int maxStudents = 30;
	private String name;
	private Course course;
	private TreeMap<Integer, Student> students = new TreeMap<Integer, Student>();
	private TreeMap<Integer, Professor> professors = new TreeMap<Integer, Professor>();
	private HashMap<String, Project> projects = new HashMap<String, Project>();
	private HashMap<Integer, Observer> invisible = new HashMap<Integer, Observer>();

	public Discipline(String n, Course c){
		this.name = n;
		this.course = c;
	}

	public void addStudent(Student student) throws TooManyStudentsException{
		if(this.students.size() == this.maxStudents){
			throw new TooManyStudentsException();
		}

		this.students.put(student.getId(), student);
	}

	public void addProfessor(Professor professor){
		this.professors.put(professor.getId(), professor);
	}

	public String getName(){
		return this.name;
	}

	public Course getCourse(){
		return this.course;
	}

	public String getCourseName(){
		return this.course.getName();
	}

	public List<String> showStudents(){
		List<String> results = new ArrayList<String>();

		for(Student student: this.students.values()){
			results.add(student.print(DefaultPrinter.INSTANCE));
		}

		return results;
	}

	public void deliverProject(int id, String projectName, String message) throws InvalidProjectException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		project.deliver(id, message);
	}

	public void createProject(String projectName) throws InvalidProjectException{
		Project project = this.getProject(projectName);

		if(project != null){
			throw new InvalidProjectException();
		}

		this.projects.put(projectName, new Project(this, projectName));
	}

	public void closeProject(String projectName) throws InvalidProjectException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		project.close();
	}

	public List<String> showProjectSubmissions(String projectName) throws InvalidProjectException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		return project.showSubmissions(this.name);
	}

	public void createSurvey(String projectName) throws InvalidProjectException, SurveyException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		project.createSurvey();
	}

	public List<String> showSurveyResults(Person person, String projectName) throws InvalidProjectException, SurveyException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		// Verification needed
		String status = project.getSurveyStatus(person); 

		List<String> results = new ArrayList<String>();
		
		results.add(this.name + " - " + projectName + " " + status);

		// If status is not finished, return
		if(status != ""){
			return results;
		}

		List<Integer> data = project.gatherSurveyResults();

		if(person.isStudent()){
			results.add(" * Número de respostas: " + data.get(3));
			results.add(" * Tempo médio (horas): " + data.get(2));
		}else{
			results.add(" * Número de submissões: " + data.get(4));
			results.add(" * Número de respostas: " + data.get(3));
			results.add(" * Tempos de resolução (horas) (mínimo, médio, máximo): " + data.get(0) + ", " + data.get(2) + ", " + data.get(1));
		}

		return results;
	}

	public void cancelSurvey(String projectName) throws InvalidProjectException, SurveyException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		project.cancelSurvey();
	}

	public void closeSurvey(String projectName) throws InvalidProjectException, SurveyException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		project.closeSurvey();
	}

	public void finishSurvey(String projectName) throws InvalidProjectException, SurveyException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		project.finishSurvey();
	}

	public void openSurvey(String projectName) throws InvalidProjectException, SurveyException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		project.openSurvey();
	}

	public void answerSurvey(int id, String projectName, int hours, String comment) throws InvalidProjectException, SurveyException{
		Project project = this.getProject(projectName);

		if(project == null){
			throw new InvalidProjectException();
		}

		project.answerSurvey(id, hours, comment);
	}

	public List<String> showSurveys(){
		TreeMap<String, Project> sorted = new TreeMap<String, Project>(Collator.getInstance(Locale.getDefault()));
		
		sorted.putAll(this.projects);

		List<String> results = new ArrayList<String>();

		for(Project project: sorted.values()){
			String status;

			try{
				status = project.getSurveyStatus();
			}catch(SurveyException e){
				// Simply ignore
				continue;
			}

			String message = this.name + " - " + project.getName();

			// If status is not finished
			if(status != ""){
				results.add(message + " " + status);
				continue;
			}

			List<Integer> data = project.gatherSurveyResults();

			results.add(message + " - " + data.get(3) + " respostas - " + data.get(2) + " horas"); 
		}
		
		return results;
	}

	public void addObserver(Observer observer){
		this.invisible.remove(observer.getId());
	}

	public void removeObserver(Observer observer){
		this.invisible.put(observer.getId(), observer);
	}

	public void notifyObservers(String notification){
		HashMap<Integer, Observer> observers = new HashMap<Integer, Observer>();

		for(Student student: this.students.values()){
			observers.put(student.getId(), student);
		}

		for(Student student: this.course.getRepresentatives()){
			observers.put(student.getId(), student);
		}

		for(Professor professor: this.professors.values()){
			observers.put(professor.getId(), professor);
		}

		for(Observer observer: this.invisible.values()){
			observers.remove(observer.getId());
		}

		for(Observer observer: observers.values()){
			observer.addNotification(notification);
		}
	}

	private Project getProject(String project){
		return this.projects.get(project);
	}

	public String print(Printer printer){
		return printer.print(this);
	}
}
