package sth;

import java.util.List;
import java.util.ArrayList;

import sth.exceptions.SurveyException;

class ClosedState extends SurveyState{
	private static final long serialVersionUID = 201812040124L;

	public ClosedState(Survey survey){
		super(survey);
	}

	public void cancel(){
		this.survey.setState(new OpenedState(this.survey));
	}

	public void open() throws SurveyException{
		this.survey.setState(new OpenedState(this.survey));
	}

	public void close(){
		// left blank on purpose
	}

	public void finish(){
		this.survey.setState(new FinishedState(this.survey));
	}

	public String getStatus(){
		return "(fechado)";
	}
}
