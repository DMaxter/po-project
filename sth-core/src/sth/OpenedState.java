package sth;

import java.util.List;
import java.util.ArrayList;

import sth.exceptions.SurveyException;
import sth.exceptions.AnsweredSurveyException;

class OpenedState extends SurveyState{
	private static final long serialVersionUID = 201812040124L;

	public OpenedState(Survey survey){
		super(survey);
		String notification = "Pode preencher inquérito do projecto " + this.survey.getProjectName() + " da disciplina " + this.survey.getDisciplineName();
		this.survey.notifyObservers(notification);
	}

	public void cancel() throws SurveyException{
		if(this.survey.hasAnswers()){
			throw new AnsweredSurveyException();
		}else{
			this.survey.getProject().removeSurvey();
		}
	}

	public void open() throws SurveyException{
		throw new SurveyException();
	}

	public void close() throws SurveyException{
		this.survey.setState(new ClosedState(this.survey));
	}

	public void finish() throws SurveyException{
		throw new SurveyException();
	}

	public void answer(int id, int hours, String comment){
		this.survey.addAnswer(id, hours, comment);
	}

	public String getStatus(){
		return "(aberto)";
	}
}
