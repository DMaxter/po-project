package sth;

interface Printer{
	public String print(Person person);
	public String print(Discipline discipline);
}
