package sth;

import java.util.TreeMap;
import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;

import sth.exceptions.InvalidProjectException;
import sth.exceptions.SurveyException;
import sth.exceptions.NoSurveyAssociatedException;

/*
 * Project implementation
 */

class Project implements Serializable{
	private static final long serialVersionUID = 201811142040L;

	private String name;
	private String description;
	private boolean open = true;
	private Discipline discipline;
	private Survey survey;
	private TreeMap<Integer,String> submissions = new TreeMap<Integer, String>();

	public Project(Discipline d, String n){
		this.discipline = d;
		this.name = n;
	}

	public void close(){
		this.open = false;

		try{
			if(this.survey != null){
				this.survey.open();
			}
		}catch(SurveyException e){
			// This exception is never throw in this context
		}
	}

	public String getName(){
		return this.name;
	}

	public String getDisciplineName(){
		return this.discipline.getName();
	}

	public void deliver(Integer id, String message) throws InvalidProjectException{
		if(this.open == false){
			throw new InvalidProjectException();
		}

		this.submissions.put(id, message);
	}

	public List<String> showSubmissions(String discipline){
		List<String> results = new ArrayList<String>();

		results.add(discipline + " - " + this.name);

		for(int key: this.submissions.keySet()){
			results.add("* " + key + " - " + this.submissions.get(key));
		}

		return results;
	}

	public void createSurvey() throws InvalidProjectException, SurveyException{
		if(this.open == false){
			throw new InvalidProjectException();
		}else if(this.survey != null){
			throw new SurveyException();
		}

		this.survey = new Survey(this);
	}

	public String getSurveyStatus() throws SurveyException{
		if(this.survey == null){
			throw new SurveyException();
		}

		return this.survey.getStatus();
	}

	public String getSurveyStatus(Person person) throws InvalidProjectException, SurveyException{
		if(person.isStudent() && this.submissions.get(person.getId()) == null){
			throw new InvalidProjectException();
		}

		return this.getSurveyStatus();
	}

	public List<Integer> gatherSurveyResults(){
		List <Integer> results = this.survey.gatherResults();

		results.add(this.submissions.size());

		return results;
	}

	public void cancelSurvey() throws SurveyException{
		if(this.survey == null){
			throw new NoSurveyAssociatedException();
		}

		this.survey.cancel();
	}

	public void closeSurvey() throws SurveyException{
		if(this.survey == null){
			throw new NoSurveyAssociatedException();
		}

		this.survey.close();
	}

	public void finishSurvey() throws SurveyException{
		if(this.survey == null){
			throw new NoSurveyAssociatedException();
		}

		this.survey.finish();
	}

	public void openSurvey() throws SurveyException{
		if(this.survey == null){
			throw new NoSurveyAssociatedException();
		}else if(this.open){
			throw new SurveyException();
		}

		this.survey.open();
	}

	public void answerSurvey(int id, int hours, String comment) throws InvalidProjectException, SurveyException{
		if(this.submissions.get(id) == null){
			throw new InvalidProjectException();
		}else if(this.survey == null){
			throw new SurveyException();
		}

		this.survey.answer(id, hours, comment);
	}

	public void removeSurvey(){
		this.survey = null;
	}

	public void notifyObservers(String notification){
		this.discipline.notifyObservers(notification);
	}
}
