package sth;

interface Observer{
	public void addNotification(String notification);
	public int getId();
}
