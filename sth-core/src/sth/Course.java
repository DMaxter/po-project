package sth;

import java.util.HashMap;
import java.util.List;
import java.util.Collection;
import java.io.Serializable;

import sth.exceptions.TooManyRepresentativesException;
import sth.exceptions.InvalidDisciplineException;
import sth.exceptions.InvalidProjectException;
import sth.exceptions.SurveyException;

/*
 *  Course implementation
 */
class Course implements Serializable{
	private static final long serialVersionUID = 201811142040L;
	private static final int MAX_REPRESENTATIVES = 7;

	private String name;
	private HashMap<String, Discipline> disciplines = new HashMap<String, Discipline>();
	private HashMap<Integer, Student> representatives = new HashMap<Integer, Student>();

	public Course(String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}

	public Collection<Discipline> getDisciplines(){
		return this.disciplines.values();
	}

	public void addDiscipline(Discipline discipline){
		if(this.disciplines.get(discipline.getName()) == null){
			this.disciplines.put(discipline.getName(), discipline);
		}
	}

	public Collection<Student> getRepresentatives(){
		return this.representatives.values();
	}

	public void addRepresentative(Student student) throws TooManyRepresentativesException{
		if(this.representatives.size() == MAX_REPRESENTATIVES){
			throw new TooManyRepresentativesException();
		}

		this.representatives.put(student.getId(), student);
	}

	public void removeRepresentative(Student student){
		this.representatives.remove(student.getId());
	}

	public boolean isRepresentative(Student student){
		return (this.representatives.get(student.getId()) != null);
	}

	public void createSurvey(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		discipline.createSurvey(project);
	}

	public void cancelSurvey(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}
		
		discipline.cancelSurvey(project);
	}

	public void closeSurvey(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		discipline.closeSurvey(project);
	}

	public void finishSurvey(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		discipline.finishSurvey(project);
	}

	public void openSurvey(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		discipline.openSurvey(project);
	}

	public List<String> showDisciplineSurveys(String disciplineName) throws InvalidDisciplineException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		return discipline.showSurveys();
	}

	private Discipline getDiscipline(String discipline){
		return this.disciplines.get(discipline);
	}
}
