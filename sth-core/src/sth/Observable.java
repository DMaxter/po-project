package sth;

interface Observable{
	public void notifyObservers(String notification);
	public void addObserver(Observer observer);
	public void removeObserver(Observer observer);
}
