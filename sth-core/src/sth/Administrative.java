package sth;

/*
 * Administrative implementation
 */
class Administrative extends Person{
	private static final long serialVersionUID = 201811142040L;

	public Administrative(String name, int id, String phone){
		super(name, id, phone);
	}

	public boolean isAdministrative(){
		return true;
	}
}
