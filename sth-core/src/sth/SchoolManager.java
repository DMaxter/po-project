package sth;

import sth.exceptions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Collection;
import java.util.TreeMap;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

/**
 * The façade class.
 */
public class SchoolManager {

	private School school = new School();
	private String file;
	private boolean changes = false;
	private int login;

	/**
	 * @param datafile
	 * @throws ImportFileException
	 */
	public void importFile(String datafile) throws ImportFileException{
		try {
			school.importFile(datafile);
		} catch (IOException | BadEntryException | OverflowException e) {
			throw new ImportFileException(e);
		}

		this.changes = true;
	}

	public List<String> loadSchool(String file) throws IOException, NoSuchPersonIdException, ClassNotFoundException{
		School old = this.school;

		ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
		this.school = (School) in.readObject();
		in.close();

		try{
			List<String> notifications = this.login(this.login);

			this.file = file;
			
			if(notifications.size() == 0){
				this.changes = false;
			}else{
				this.changes = true;
			}

			return notifications;
		}catch (NoSuchPersonIdException e){
			this.school = old;
			throw e;
		}
	}

	public void saveSchool() throws IOException, NoFileException{
		if(this.file == null){
			throw new NoFileException();
		}

		this.saveSchool(this.file);
	}

	public void saveSchool(String file) throws IOException{
			this.file = file;

			ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(this.file)));
			out.writeObject(this.school);
			out.close();

			this.changes = false;
	}

	/**
	 * @param id
	 * @throws NoSuchPersonIdException
	 */
	public List<String> login(int id) throws NoSuchPersonIdException {
		this.login = this.school.login(id);

		return this.school.getNotifications(this.login);
	}

	/**
	 * @return true when the currently logged in person is an administrative
	 */
	public boolean hasAdministrative() {
		return this.school.isAdministrative(this.login);
	}

	/**
	 * @return true when the currently logged in person is a professor
	 */
	public boolean hasProfessor() {
		return this.school.isProfessor(this.login);
	}

	/**
	 * @return true when the currently logged in person is a student
	 */
	public boolean hasStudent() {
		return this.school.isStudent(this.login);
	}

	/**
	 * @return true when the currently logged in person is a representative
	 */
	public boolean hasRepresentative() {
		return this.school.isRepresentative(this.login);
	}

	public String showPerson(){
		return this.school.showPerson(this.login);
	}

	public int getLogged(){
		return this.login;
	}

	public boolean hasChanges(){
		return this.changes;
	}

	public void changePhoneNumber(String phone){
		this.school.changePhoneNumber(this.login, phone);

		this.changes = true;
	}

	public Collection<String> searchPerson(String person){
		return this.school.searchPerson(person);
	}

	public Collection<String> showAllPersons(){
		return this.school.getAllPeople();
	}

	public List<String> showDisciplineStudents(String discipline) throws InvalidDisciplineException{
		return this.school.showDisciplineStudents(this.login, discipline);
	}

	public void createProject(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException{
		this.school.createProject(this.login, discipline, project);

		this.changes = true;
	}

	public void closeProject(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException{
		this.school.closeProject(this.login, discipline, project);

		this.changes = true;
	}

	public void deliverProject(String discipline, String project, String message) throws InvalidDisciplineException, InvalidProjectException{
		this.school.deliverProject(this.login, discipline, project, message);
		this.changes = true;
	}

	public List<String> showProjectSubmissions(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException{
		return this.school.showProjectSubmissions(this.login, discipline, project);
	}

	public void createSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.school.createSurvey(this.login, discipline, project);

		this.changes = true;
	}

	public List<String> showSurveyResults(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		return this.school.showSurveyResults(this.login, discipline, project);
	}

	public void cancelSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.school.cancelSurvey(this.login, discipline, project);
		this.changes = true;
	}

	public void closeSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.school.closeSurvey(this.login, discipline, project);
		this.changes = true;
	}

	public void finishSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.school.finishSurvey(this.login, discipline, project);
		this.changes = true;
	}

	public void openSurvey(String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.school.openSurvey(this.login, discipline, project);
		this.changes = true;
	}

	public void answerSurvey(String discipline, String project, int hours, String comment) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		this.school.answerSurvey(this.login, discipline, project, hours, comment);
		this.changes = true;
	}

	public List<String> showDisciplineSurveys(String discipline) throws InvalidDisciplineException{
		return this.school.showDisciplineSurveys(this.login, discipline);
	}
}
