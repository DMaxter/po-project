package sth;

import sth.exceptions.*;

import java.util.List;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.HashMap;
import java.util.Collection;
import java.util.Locale;
import java.text.Collator;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Serializable;

/**
 * School implementation.
 */
class School implements Serializable {

	/** Serial number for serialization. */
	private static final long serialVersionUID = 201810051538L;

	private HashMap<String, Course> courses = new HashMap<String, Course>();
	private TreeMap<Integer, Student> students = new TreeMap<Integer, Student>();
	private TreeMap<Integer, Professor> professors = new TreeMap<Integer, Professor>();
	private TreeMap<Integer, Administrative> administratives = new TreeMap<Integer, Administrative>();
  
	/**
	 * Create school and its entities from file
	 *
	 * @param filename file for creating school
	 * @throws IOException if something wrong occurs
	 * @throws BadEntryException if an entity is unknown
	 * @throws OverflowException if an entity has values on excess
	 */

	void importFile(String filename) throws IOException, BadEntryException, OverflowException{
		BufferedReader file = new BufferedReader(new FileReader(filename));
		String line;

		Person last = null;
		boolean representative = false;

		if((line = file.readLine()) == null){
			return;
		}

		do{
			String[] params = line.split("\\|");

			if(params[0].equals("DELEGADO")){
				last = new Student(params[3], Integer.parseInt(params[1]), params[2]);
				
				representative = true;
				this.addStudent((Student) last);

			}else if(params[0].equals("ALUNO")){
				last = new Student(params[3], Integer.parseInt(params[1]), params[2]);

				this.addStudent((Student) last);

			}else if(params[0].equals("DOCENTE")){
				last =  new Professor(params[3], Integer.parseInt(params[1]), params[2]);

				this.addProfessor((Professor) last);
				
			}else if(params[0].equals("FUNCIONÁRIO")){
				last = new Administrative(params[3], Integer.parseInt(params[1]), params[2]);

				this.addAdministrative((Administrative) last);
				last = null;

			}else if((params[0].split(" "))[0].equals("#") && last != null){
				Course c = this.searchCourse((params[0].split(" "))[1]);

				if(c == null){
					c = new Course((params[0].split(" "))[1]);
					this.courses.put(c.getName(), c);
				}
				
				Discipline d = searchDiscipline(params[1], c);

				if(d == null){
					d = new Discipline(params[1], c);
					c.addDiscipline(d);
				}

				if(last.isStudent()){
					d.addStudent((Student) last);
					((Student) last).setCourse(c);
					((Student) last).addDiscipline(d);
				}else if(last.isProfessor()){
					d.addProfessor((Professor) last);
					((Professor) last).addDiscipline(d);
				}else{
					throw new BadEntryException("Esta classe nunca tem disciplinas associadas");
				}

				if(representative){
					c.addRepresentative((Student) last);
					representative = false;
				}
			}else{
				throw new BadEntryException("Tipo de pessoa não encontrado");
			}
		}while((line = file.readLine()) != null);
	}
  
	/**
	 * Login the user for the current school
	 *
	 * @param id the id of the user to login
	 * @return the id of the logged person
	 * @throws NoSuchPersonIdException if teh user is not present in the school
	 */
	public int login(int id) throws NoSuchPersonIdException{
		Person person = this.searchPerson(id);

		if(person == null){
			throw new NoSuchPersonIdException(id);
		}

		return person.getId();
	}

	/**
	 * Change the phone number of a person
	 *
	 * @param id the id of the user to change the phone number
	 * @param phone the new phone number
	 */
	public void changePhoneNumber(int id, String phone){
		Person person = this.searchPerson(id);
		person.setPhone(phone);
	}

	/**
	 * Search for the given course in all known courses
	 *
	 * @param name the name of the course to search for
	 * @return the course whose name matches the given one, else return null
	 */
	private Course searchCourse(String name){
		return this.courses.get(name);
	}

	/**
	 * Search for a discipline in a course
	 *
	 * @param name the name of the discipline to search for
	 * @param course the course to the search for the discipline
	 * @return null if no discipline was found or the discipline otherwise
	 */
	private Discipline searchDiscipline(String name, Course course){
		Collection<Discipline> disciplines = course.getDisciplines();

		for(Discipline discipline : disciplines){
			if(name.equals(discipline.getName())){
				return discipline;
			}
		}

		return null;
	}

	/**
	 * Adds a student to the current school
	 *
	 * @param student the student to add
	 */
	private void addStudent(Student student){
		this.students.put(student.getId(), student);
	}
	
	/**
	 * Adds a professor to the current school
	 *
	 * @param professor the professor to add
	 */
	private void addProfessor(Professor professor){
		this.professors.put(professor.getId(), professor);
	}

	/**
	 * Adds an administrative to the current school
	 *
	 * @param administrative the administrative to add
	 */
	private void addAdministrative(Administrative administrative){
		this.administratives.put(administrative.getId(), administrative);
	}

	/**
	 * Get the professor with the given id
	 *
	 * @param id the id of the professor to search for
	 * @return the professor with the given id
	 */
	private Professor getProfessor(int id){
		return this.professors.get(id);
	}

	/**
	 * Get the student with the given id
	 *
	 * @param id the id of the student to search for
	 * @return the professor with the given id
	 */
	private Student getStudent(int id){
		return this.students.get(id);
	}

	/**
	 * Get the administrative with the given id
	 *
	 * @param id the id of the administrative to search for
	 * @return the administrative with the given id
	 */
	private Administrative getAdministrative(int id){
		return this.administratives.get(id);
	}

	/**
	 * Show the details of the person with the given id
	 *
	 * @param id the id of the person to show details
	 * @return the string showing the details of the person
	 */
	public String showPerson(int id){
		Person person = this.searchPerson(id);

		return person.print(DefaultPrinter.INSTANCE);
	}

	/**
	 * Search for a person (by id)
	 *
	 * @param id the id of the person to search for
	 * @return the person with the given id
	 */
	public Person searchPerson(int id){
		if(this.isStudent(id)){
			return this.getStudent(id);
		}else if(this.isProfessor(id)){
			return this.getProfessor(id);
		}else if(this.isAdministrative(id)){
			return this.getAdministrative(id);
		}

		return null;
	}

	/**
	 * Search for a person (by name)
	 *
	 * @param name part of the name of the person to search for
	 * @return a collection containing all persons whose name matches the given one
	 */
	public Collection<String> searchPerson(String name){
		TreeMap<String, String> results = new TreeMap<String, String>(Collator.getInstance(Locale.getDefault()));

		for(Student student: this.students.values()){
			if(student.getName().contains(name)){
				results.put(student.getName(), student.print(DefaultPrinter.INSTANCE));
			}
		}

		for(Professor professor: this.professors.values()){
			if(professor.getName().contains(name)){
				results.put(professor.getName(), professor.print(DefaultPrinter.INSTANCE));
			}
		}

		for(Administrative administrative: this.administratives.values()){
			if(administrative.getName().contains(name)){
				results.put(administrative.getName(), administrative.print(DefaultPrinter.INSTANCE));
			}
		}

		return results.values();
	}

	/**
	 * Test if a person with a given ID is a student
	 *
	 * @param id the ID to test
	 * @return true if the person with the given ID is a student
	 */
	public boolean isStudent(int id){
		return (this.students.get(id) != null);
	}

	/**
	 * Test if a person with a given ID is a professor
	 *
	 * @param id the ID to test
	 * @return true if the person with the given ID is a professor
	 */
	public boolean isProfessor(int id){
		return (this.professors.get(id) != null);
	}

	/**
	 * Test if a person with a given ID is an administrative
	 *
	 * @param id the ID to test
	 * @return true if the person with the given ID is an administrative
	 */
	public boolean isAdministrative(int id){
		return (this.administratives.get(id) != null);
	}


	/**
	 * Test if a person with a given ID is a representative
	 *
	 * @param id the ID to test
	 * @return true if the person with the given ID is a representative
	 */
	public boolean isRepresentative(int id){
		return (this.isStudent(id) && (this.getStudent(id)).isRepresentative());
	}

	/**
	 * Get all the people that the current school knows of
	 *
	 * @return a collection containing all people
	 */
	public Collection<String> getAllPeople(){
		TreeMap<Integer, String> people = new TreeMap<Integer, String>();
		for(Student student: this.students.values()){
			people.put(student.getId(), student.print(DefaultPrinter.INSTANCE));
		}

		for(Professor professor: this.professors.values()){
			people.put(professor.getId(), professor.print(DefaultPrinter.INSTANCE));
		}

		for(Administrative administrative: this.administratives.values()){
			people.put(administrative.getId(), administrative.print(DefaultPrinter.INSTANCE));
		}

		return people.values();
	}

	/**
	 * Send a submission for the given project
	 *
	 * @param id the id of the student delivering the project
	 * @param discipline the name of the discipline to deliver project
	 * @param project the name of the project
	 * @param message the message given by the student
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws InsufficientPermissionsException it the person is not a student
	 */
	public void deliverProject(int id, String discipline, String project, String message) throws InvalidDisciplineException, InvalidProjectException{
		if(this.isStudent(id) == false){
			throw new InsufficientPermissionsException();
		}

		Student student = this.getStudent(id);

		student.deliverProject(discipline, project, message);
	}

	/**
	 * Create the given project
	 *
	 * @param id the id of the person creating the project
	 * @param discipline the name of the discipline to create the project
	 * @param project the name of the project
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project already exists
	 * @throws InsufficientPermissionsException if the user is not a professor
	 */
	public void createProject(int id, String discipline, String project) throws InvalidDisciplineException, InvalidProjectException{
		if(this.isProfessor(id) == false){
			throw new InsufficientPermissionsException();
		}

		Professor professor = this.getProfessor(id);

		professor.createProject(discipline, project);
	}

	/**
	 * Close the given project
	 *
	 * @param id the id of the person closing the project
	 * @param discipline the name of the discipline to close the project
	 * @param project the name of the project to close
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws InsufficientPermissionsException if the user is not a professor
	 */
	public void closeProject(int id, String discipline, String project) throws InvalidDisciplineException, InvalidProjectException{
		if(this.isProfessor(id) == false){
			throw new InsufficientPermissionsException();
		}
		
		Professor professor = this.getProfessor(id);
	
		professor.closeProject(discipline, project);
	}

	/**
	 * Shows the students of the given discipline
	 *
	 * @param id the id of the user requesting the operation
	 * @param discipline the name of the discipline to show the students
	 * @return a list of strings describing the students of the given discipline
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InsufficientPermissionsException if the user is not a professor
	 */
	public List<String> showDisciplineStudents(int id, String discipline) throws InvalidDisciplineException{
		if(this.isProfessor(id) == false){
			throw new InsufficientPermissionsException();
		}

		Professor professor = this.getProfessor(id);

		return professor.showDisciplineStudents(discipline);
	}

	/**
	 * Shows the submissions of the given project
	 *
	 * @param id the id of the user requesting the operation
	 * @param discipline the name of the discipline to show the project submissions
	 * @param project the name of the project
	 * @return a list of strings containing the submissions of the given project
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws InsufficientPermissionsException if the user is not a professor
	 */
	public List<String> showProjectSubmissions(int id, String discipline, String project) throws InvalidDisciplineException, InvalidProjectException{
		if(this.isProfessor(id) == false){
			throw new InsufficientPermissionsException();
		}

		Professor professor = this.getProfessor(id);

		return professor.showProjectSubmissions(discipline, project);
	}

	/**
	 * Create a survey for the given project
	 *
	 * @param id the id of the user creating the survey
	 * @param discipline the name of the discipline to create a survey
	 * @param project the name of the project
	 * @throws InvalidDisciplineException if discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws SurveyException if the survey is invalid
	 * @throws InsufficientPermissionsException if the user is not a representative
	 */
	public void createSurvey(int id, String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		if(this.isRepresentative(id) == false){
			throw new InsufficientPermissionsException();
		}

		Student student = this.getStudent(id);

		student.createSurvey(discipline, project);
	}

	/**
	 * Show the results of the given survey
	 *
	 * @param id the id of the user requesting the operation
	 * @param discipline the name of the discipline to show the survey results
	 * @param project the name of teh project associated with the survey
	 * @return a list of strings showing the status of the survey
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws SurveyException if the survey is invalid
	 * @throws InsufficientPermissionsException if the user is neither a student or a professor
	 */
	public List<String> showSurveyResults(int id, String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		if(this.isStudent(id)){
			Student student = this.getStudent(id);

			return student.showSurveyResults(discipline, project);
		}else if(this.isProfessor(id)){
			Professor professor = this.getProfessor(id);

			return professor.showSurveyResults(discipline, project);
		}else{
			throw new InsufficientPermissionsException();
		}
	}

	/**
	 * Cancel the given survey
	 *
	 * @param id the id of the user cancelling the survey
	 * @param discipline the name of the discipline to cancel the survey
	 * @param project the name of the project associated with the survey
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws SurveyException if the survey is invalid
	 * @throws InsufficientPermissionsException if the user is not a representative
	 */
	public void cancelSurvey(int id, String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		if(this.isRepresentative(id) == false){
			throw new InsufficientPermissionsException();
		}

		Student student = this.getStudent(id);

		student.cancelSurvey(discipline, project);
	}

	/**
	 * Close the given survey
	 *
	 * @param id the id of the user closing the survey
	 * @param discipline the name of the discipline to close the survey
	 * @param project the name of the project associated with the survey
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws SurveyException if the survey is invalid
	 * @throws InsufficientPermissionsException if the user is not a representative
	 */
	public void closeSurvey(int id, String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		if(this.isRepresentative(id) == false){
			throw new InsufficientPermissionsException();
		}

		Student student = this.getStudent(id);

		student.closeSurvey(discipline, project);
	}

	/**
	 * Finish the given survey
	 *
	 * @param id the id of the user finishing the survey
	 * @param discipline the name of the discipline to finish the survey
	 * @param project the name of the project associated with the survey
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws SurveyException if the survey is invalid
	 * @throws InsufficientPermissionsException if the user is not a representative
	 */
	public void finishSurvey(int id, String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		if(this.isRepresentative(id) == false){
			throw new InsufficientPermissionsException();
		}

		Student student = this.getStudent(id);

		student.finishSurvey(discipline, project);
	}

	/**
	 * Opens the given survey
	 *
	 * @param id the id of the user opening the survey
	 * @param discipline the name of the discipline to open the survey
	 * @param project the name of the project associated with the survey
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws SurveyException if the survey is invalid
	 * @throws InsufficientPermissionsException if the user is not a representative
	 */
	public void openSurvey(int id, String discipline, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		if(this.isRepresentative(id) == false){
			throw new InsufficientPermissionsException();
		}

		Student student = this.getStudent(id);

		student.openSurvey(discipline, project);
	}

	/**
	 * Answer the given survey
	 *
	 * @param id the id of the user answering the survey
	 * @param discipline the name of the discipline associated with the survey
	 * @param project the name of the project associated with the survey
	 * @param hours the amount of hours spent on the project
	 * @param comment the comment of the user
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InvalidProjectException if the project is invalid
	 * @throws SurveyException if the survey is invalid
	 * @throws InsufficientPermissionsException if the user is not a student
	 */
	public void answerSurvey(int id, String discipline, String project, int hours, String comment) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		if(this.isStudent(id) == false){
			throw new InsufficientPermissionsException();
		}

		Student student = this.getStudent(id);

		student.answerSurvey(id, discipline, project, hours, comment);
	}

	/**
	 * Show all surveys of a discipline
	 *
	 * @param id the id of the user requesting the operation
	 * @param discipline the name of the discipline to show the surveys
	 * @return a  list of strings showing the results of all surveys of a discipline
	 * @throws InvalidDisciplineException if the discipline is invalid
	 * @throws InsufficientPermissionsException if the user is not a representative
	 */
	public List<String> showDisciplineSurveys(int id, String discipline) throws InvalidDisciplineException{
		if(this.isRepresentative(id) == false){
			throw new InsufficientPermissionsException();
		}

		Student student = this.getStudent(id);

		return student.showDisciplineSurveys(discipline);
	}

	/**
	 * Get the notifications of a person
	 *
	 * @param id the id of the user to get notifications
	 * @return a list of the unread notifications
	 */
	public List<String> getNotifications(int id){
		Person person = this.searchPerson(id);

		return person.getNotifications();
	}
}
