package sth;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.Serializable;

import sth.exceptions.SurveyException;

/*
 * Survey implementation
 */

class Survey implements Serializable{	
	private static final long serialVersionUID = 201811142040L;
	
	private SurveyState status = new CreatedState(this);
	private Project project;
	private int total = 0;
	private int min = Integer.MAX_VALUE;
	private int max = 0;
	private int submissions = 0;
	private HashMap<Integer, Integer> students = new HashMap<Integer, Integer>();

	public Survey(Project project){
		this.project = project;
	}

	public Project getProject(){
		return this.project;
	}

	public void setState(SurveyState status){
		this.status = status;
	}

	public String getStatus(){
		return this.status.getStatus();
	}

	public void cancel() throws SurveyException{
		this.status.cancel();
	}

	public void open() throws SurveyException{
		this.status.open();
	}

	public void close() throws SurveyException{
		this.status.close();
	}

	public void finish() throws SurveyException{
		this.status.finish();
	}

	public List<Integer> gatherResults(){
		return this.status.gatherResults();
	}

	public void answer(int id, int hours, String comment) throws SurveyException{
		this.status.answer(id, hours, comment);
	}

	public String getProjectName(){
		return this.project.getName();
	}

	public String getDisciplineName(){
		return this.project.getDisciplineName();
	}

	public void addAnswer(int id, int hours, String comment){
		if(this.students.get(id) == null){
			submissions++;
			total += hours;
			
			if(max < hours){
				max = hours;
			}

			if(min > hours){
				min = hours;
			}

			this.students.put(id, id);
		}
	}

	public boolean hasAnswers(){
		return (this.submissions != 0); 
	}

	public void notifyObservers(String notification){
		this.project.notifyObservers(notification);
	}

	public int getMin(){
		if(this.min == Integer.MAX_VALUE){
			return 0;
		}
		
		return this.min;
	}

	public int getMax(){
		return this.max;
	}

	public int getAvg(){
		if(this.submissions == 0){
			return 0;
		}

		return this.total / this.submissions;
	}

	public int getSubmissions(){
		return this.submissions;
	}
}
