package sth;

import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.text.Collator;

import sth.exceptions.InvalidDisciplineException;
import sth.exceptions.InvalidProjectException;
import sth.exceptions.SurveyException;

/*
 * Professor implementation
 */

class Professor extends Person{
	private static final long serialVersionUID = 201811142040L;

	private List<Discipline> disciplines = new ArrayList<Discipline>();

	public Professor(String name, int id, String phone){
		super(name, id, phone);
	}

	public void addDiscipline(Discipline discipline){
		this.disciplines.add(discipline);
	}

	public void createProject(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		discipline.createProject(project);
	}

	public void closeProject(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		discipline.closeProject(project);
	}

	public List<String> showDisciplineStudents(String disciplineName) throws InvalidDisciplineException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		return discipline.showStudents();
	}

	public List<String> showProjectSubmissions(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		return discipline.showProjectSubmissions(project);
	}

	public List<String> showSurveyResults(String disciplineName, String project) throws InvalidDisciplineException, InvalidProjectException, SurveyException{
		Discipline discipline = this.getDiscipline(disciplineName);

		if(discipline == null){
			throw new InvalidDisciplineException();
		}

		return discipline.showSurveyResults(this, project);
	}

	private Discipline getDiscipline(String disciplineName){
		for(Discipline discipline: this.disciplines){
			if(discipline.getName().equals(disciplineName)){
				return discipline;
			}
		}

		return null;
	}

	public String printDisciplines(Printer printer){
		String message = "";

		List<String> disciplines = new ArrayList<String>();

		for(Discipline discipline: this.disciplines){
			 disciplines.add(discipline.print(printer));
		}

		disciplines.sort(Collator.getInstance(Locale.getDefault()));
		
		for(String s: disciplines){
			message += s + "\n";
		}

		return message.trim();
	}

	public boolean isProfessor(){
		return true;
	}
}
