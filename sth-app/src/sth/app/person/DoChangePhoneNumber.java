package sth.app.person;

import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.Input;
import sth.SchoolManager;
/**
 * 4.2.2. Change phone number.
 */
public class DoChangePhoneNumber extends Command<SchoolManager> {
	Input<String> phone;

	/**
	 * @param receiver
	 */
	public DoChangePhoneNumber(SchoolManager receiver) {
		super(Label.CHANGE_PHONE_NUMBER, receiver);
		this.phone = _form.addStringInput(Message.requestPhoneNumber());
	}

	/** @see pt.tecnico.po.ui.Command#execute() */
	@Override
	public final void execute() {
		_form.parse();
		_receiver.changePhoneNumber(this.phone.value());
		_display.popup(_receiver.showPerson());
	}

}
