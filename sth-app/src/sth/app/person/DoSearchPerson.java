package sth.app.person;

import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.Input;
import sth.SchoolManager;

import java.util.Collection;

/**
 * 4.2.4. Search person.
 */
public class DoSearchPerson extends Command<SchoolManager> {
	Input<String> name;

	/**
	 * @param receiver
	 */
	public DoSearchPerson(SchoolManager receiver) {
		super(Label.SEARCH_PERSON, receiver);
		this.name = _form.addStringInput(Message.requestPersonName());
	}

	/** @see pt.tecnico.po.ui.Command#execute() */
	@Override
	public final void execute() {
		_form.parse();
		Collection<String> people = _receiver.searchPerson(this.name.value());
		
		for(String s: people){
			_display.addLine(s);
		}

		_display.display();
	}

}
