package sth.app.teaching;

import java.util.List;

import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.DialogException;
import pt.tecnico.po.ui.Input;
import sth.exceptions.InvalidDisciplineException;
import sth.exceptions.InvalidProjectException;
import sth.app.exceptions.NoSuchDisciplineException;
import sth.app.exceptions.NoSuchProjectException;
import sth.SchoolManager;

/**
 * 4.3.3. Show project submissions.
 */
public class DoShowProjectSubmissions extends Command<SchoolManager> {
	Input<String> discipline;
	Input<String> project;

	/**
	 * @param receiver
	 */
	public DoShowProjectSubmissions(SchoolManager receiver) {
		super(Label.SHOW_PROJECT_SUBMISSIONS, receiver);
		this.discipline = _form.addStringInput(Message.requestDisciplineName());
		this.project = _form.addStringInput(Message.requestProjectName());
	}

	/** @see pt.tecnico.po.ui.Command#execute() */
	@Override
	public final void execute() throws DialogException {
		_form.parse();

		try{
			List<String> results = _receiver.showProjectSubmissions(this.discipline.value(), this.project.value());

			for(String s: results){
				_display.addLine(s);
			}

		}catch(InvalidDisciplineException e){
			throw new NoSuchDisciplineException(this.discipline.value());
		}catch(InvalidProjectException e){
			throw new NoSuchProjectException(this.discipline.value(), this.project.value());
		}

		_display.display();
	}

}
