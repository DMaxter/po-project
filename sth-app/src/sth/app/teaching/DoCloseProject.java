package sth.app.teaching;

import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.DialogException;
import pt.tecnico.po.ui.Input;
import sth.SchoolManager;
import sth.app.exceptions.NoSuchProjectException;
import sth.app.exceptions.NoSuchDisciplineException;
import sth.exceptions.InvalidDisciplineException;
import sth.exceptions.InvalidProjectException;

/**
 * 4.3.2. Close project.
 */
public class DoCloseProject extends Command<SchoolManager> {
	Input<String> discipline;
	Input<String> project;

	/**
	 * @param receiver
	 */
	public DoCloseProject(SchoolManager receiver) {
		super(Label.CLOSE_PROJECT, receiver);
		this.discipline = _form.addStringInput(Message.requestDisciplineName());
		this.project = _form.addStringInput(Message.requestProjectName());
	}

	/** @see pt.tecnico.po.ui.Command#execute() */
	@Override
	public final void execute() throws DialogException {
		_form.parse();
		
		try{
		_receiver.closeProject(this.discipline.value(), this.project.value());
		}catch(InvalidDisciplineException e){
			throw new NoSuchDisciplineException(this.discipline.value());
		}catch(InvalidProjectException e){
			throw new NoSuchProjectException(this.discipline.value(), this.project.value());
		}
	}
}
