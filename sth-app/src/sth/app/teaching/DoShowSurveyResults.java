package sth.app.teaching;

import java.util.List;

import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.DialogException;
import pt.tecnico.po.ui.Input;
import sth.app.exceptions.NoSuchProjectException;
import sth.app.exceptions.NoSurveyException;
import sth.app.exceptions.NoSuchDisciplineException;
import sth.exceptions.InvalidProjectException;
import sth.exceptions.InvalidDisciplineException;
import sth.exceptions.SurveyException;
import sth.SchoolManager;

/**
 * 4.3.5. Show survey results.
 */
public class DoShowSurveyResults extends Command<SchoolManager> {
	Input<String> discipline;
	Input<String> project;

	/**
	 * @param receiver
	 */
	public DoShowSurveyResults(SchoolManager receiver) {
		super(Label.SHOW_SURVEY_RESULTS, receiver);
		this.discipline = _form.addStringInput(Message.requestDisciplineName());
		this.project = _form.addStringInput(Message.requestProjectName());
	}

	/** @see pt.tecnico.po.ui.Command#execute() */
	@Override
	public final void execute() throws DialogException {
		_form.parse();

		try{
			List<String> results = _receiver.showSurveyResults(this.discipline.value(), this.project.value());

			for(String s: results){
				_display.addLine(s);
			}
		}catch(InvalidProjectException e){
			throw new NoSuchProjectException(this.discipline.value(), this.project.value());
		}catch(InvalidDisciplineException e){
			throw new NoSuchDisciplineException(this.discipline.value());
		}catch(SurveyException e){
			throw new NoSurveyException(this.discipline.value(), this.project.value());
		}

		_display.display();
	}

}
