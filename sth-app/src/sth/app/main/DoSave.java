package sth.app.main;

import java.io.IOException;

import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.Input;
import pt.tecnico.po.ui.DialogException;
import sth.SchoolManager;
import sth.app.exceptions.NoSuchPersonException;
import sth.exceptions.NoSuchPersonIdException;
import sth.exceptions.NoFileException;

/**
 * 4.1.1. Save to file under current name (if unnamed, query for name).
 */
public class DoSave extends Command<SchoolManager> {
	Input<String> file;

	/**
	 * @param receiver
	 */
	public DoSave(SchoolManager receiver) {
		super(Label.SAVE, receiver);
		this.file = _form.addStringInput(Message.newSaveAs());
	}

	/** @see pt.tecnico.po.ui.Command#execute() */
	@Override
	public final void execute() throws DialogException {
		if(_receiver.hasChanges() == false){
			return;
		}

		try{
			try{
				_receiver.saveSchool();
			}catch(NoFileException e){
				_form.parse();
				_receiver.saveSchool(this.file.value());
			}catch(IOException e){
				e.printStackTrace();
			}finally{
				_form.clear();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}

}
