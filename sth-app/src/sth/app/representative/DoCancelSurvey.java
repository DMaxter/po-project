package sth.app.representative;

import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.DialogException;
import pt.tecnico.po.ui.Input;
import sth.exceptions.NoSurveyAssociatedException;
import sth.exceptions.FinishedSurveyException;
import sth.exceptions.AnsweredSurveyException;
import sth.exceptions.InvalidDisciplineException;
import sth.exceptions.InvalidProjectException;
import sth.exceptions.SurveyException;
import sth.app.exceptions.NoSurveyException;
import sth.app.exceptions.SurveyFinishedException;
import sth.app.exceptions.NonEmptySurveyException;
import sth.app.exceptions.NoSuchDisciplineException;
import sth.app.exceptions.NoSuchProjectException;
import sth.SchoolManager;

/**
 * 4.5.2. Cancel survey.
 */
public class DoCancelSurvey extends Command<SchoolManager> {
	Input<String> discipline;
	Input<String> project;

	/**
	 * @param receiver
	 */
	public DoCancelSurvey(SchoolManager receiver) {
		super(Label.CANCEL_SURVEY, receiver);
		this.discipline = _form.addStringInput(Message.requestDisciplineName());
		this.project = _form.addStringInput(Message.requestProjectName());
	}

	/** @see pt.tecnico.po.ui.Command#execute() */
	@Override
	public final void execute() throws DialogException {
		_form.parse();

		try{
			_receiver.cancelSurvey(this.discipline.value(), this.project.value());
		}catch(NoSurveyAssociatedException e){
			throw new NoSurveyException(this.discipline.value(), this.project.value());
		}catch(FinishedSurveyException e){
			throw new SurveyFinishedException(this.discipline.value(), this.project.value());
		}catch(AnsweredSurveyException e){
			throw new NonEmptySurveyException(this.discipline.value(), this.project.value());
		}catch(InvalidDisciplineException e){
			throw new NoSuchDisciplineException(this.discipline.value());
		}catch(InvalidProjectException e){
			throw new NoSuchProjectException(this.discipline.value(), this.project.value());
		}catch(SurveyException e){
			// Will never reach this exception
			e.printStackTrace();
		}
	}

}
