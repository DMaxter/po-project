package sth.app.representative;

import pt.tecnico.po.ui.Command;
import pt.tecnico.po.ui.DialogException;
import pt.tecnico.po.ui.Input;
import sth.app.exceptions.NoSuchProjectException;
import sth.app.exceptions.NoSuchDisciplineException;
import sth.app.exceptions.DuplicateSurveyException;
import sth.exceptions.InvalidProjectException;
import sth.exceptions.InvalidDisciplineException;
import sth.exceptions.SurveyException;
import sth.SchoolManager;

/**
 * 4.5.1. Create survey.
 */
public class DoCreateSurvey extends Command<SchoolManager> {
	Input<String> discipline;
	Input<String> project;

	/**
	* @param receiver
	*/
	public DoCreateSurvey(SchoolManager receiver) {
		super(Label.CREATE_SURVEY, receiver);
		this.discipline = _form.addStringInput(Message.requestDisciplineName());
		this.project = _form.addStringInput(Message.requestProjectName());
	}

	/** @see pt.tecnico.po.ui.Command#execute() */
	@Override
	public final void execute() throws DialogException {
		_form.parse();

		try{
			_receiver.createSurvey(this.discipline.value(), this.project.value());
		}catch(InvalidDisciplineException e){
			throw new NoSuchDisciplineException(this.discipline.value());
		}catch(InvalidProjectException e){
			throw new NoSuchProjectException(this.discipline.value(), this.project.value());
		}catch(SurveyException e){
			throw new DuplicateSurveyException(this.discipline.value(), this.project.value());
		}
	}

}
